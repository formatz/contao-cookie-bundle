<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_module']['palettes']['cookie_fz'] =
    '{title_legend},name,type'
;

$GLOBALS['TL_DCA']['tl_module']['fields']['title'] = [
    'label' => 'Titre',
    'inputType' => 'text',
    'sql' => ['type' => 'string', 'length' => 255, 'default' => ''],
];

$GLOBALS['TL_DCA']['tl_module']['fields']['text'] = [
    'label' => 'Texte',
    'inputType' => 'textarea',
    'eval' => ['rows' => 4],
    'sql' => ['type' => 'text', 'default' => ''],
];

$GLOBALS['TL_DCA']['tl_module']['fields']['lpd_file'] = [
    'label' => 'Document pour la LPD Suisse (chemin)',
    'inputType' => 'text',
    'sql' => ['type' => 'string', 'length' => 255, 'default' => ''],
];

$GLOBALS['TL_DCA']['tl_module']['fields']['bg_color'] = [
    'label' => 'Couleur du fond (ex: d3d3d3)',
    'inputType' => 'text',
    'sql' => ['type' => 'string', 'length' => 255, 'default' => ''],
];

$GLOBALS['TL_DCA']['tl_module']['fields']['text_color'] = [
    'label' => 'Couleur du texte (ex: 333)',
    'inputType' => 'text',
    'sql' => ['type' => 'string', 'length' => 255, 'default' => ''],
];

PaletteManipulator::create()
    ->addLegend('text_legend', 'type', PaletteManipulator::POSITION_AFTER)
    ->addField('title', 'text_legend')
    ->addField('text', 'text_legend')
    ->addField('lpd_file', 'text_legend')
    ->applyToPalette('cookie_fz', 'tl_module')
;

PaletteManipulator::create()
    ->addLegend('style_legend', 'text', PaletteManipulator::POSITION_AFTER)
    ->addField('bg_color', 'style_legend')
    ->addField('text_color', 'style_legend')
    ->applyToPalette('cookie_fz', 'tl_module')
;

$GLOBALS['TL_DCA']['tl_module']['palettes']['cookie_fz'] .=
    ';{template_legend},customTpl'
;
