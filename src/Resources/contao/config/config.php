<?php

$GLOBALS['FE_MOD']['miscellaneous']['cookie_fz'] = 'Formatz\ContaoCookieBundle\Controller\FrontendModule\CookieFZFrontendModuleController';

// Gtag
$GLOBALS['TL_HEAD'][] = '<script async src="https://www.googletagmanager.com/gtag/js?id=G-XXXXXX"></script>';

// Assets
$GLOBALS['TL_CSS'][] = 'bundles/formatzcontaocookie/styles.css';
$GLOBALS['TL_JAVASCRIPT'][] = 'bundles/formatzcontaocookie/cookiefz.js';
