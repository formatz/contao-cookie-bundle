<?php

namespace Formatz\ContaoCookieBundle\Controller\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FrontendModule(category="miscellaneous", template="mod_cookie_fz")
 */
class CookieFZFrontendModuleController extends AbstractFrontendModuleController
{
    public bool $protected = false;

    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        return $template->getResponse();
    }

    public function generate(): string
    {
        return '<script>
            const params = {
                alwaysShowBanner: true
            }
        
            const cookieBanner = CookieFZ.create(params, function() {
                console.log("cookies acceptés")
            }, function() {
                console.log("cookies refusés")
            })
            cookieBanner.init()
            console.log(cookieBanner)
        </script>
        ';
    }
}
