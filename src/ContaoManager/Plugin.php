<?php

namespace Formatz\ContaoCookieBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;
use Formatz\ContaoCookieBundle\FormatzContaoCookieBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(FormatzContaoCookieBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
