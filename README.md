# Minimum requirements
Contao v4.12

# Installation
Installation with composer :
```
composer require formatz/contao-cookie-bundle
```
Then, update the database :
```
vendor/bin/contao-console contao:migrate
```

# Usage
Add the module to the page. Module name is "cookie_fz".
You can customize the style, texts, etc.
